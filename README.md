# LED Triangle Light
![](led_example.gif)


This repo contains everything you need to build this triangle! The video is from an older version, the light effects got improved. You can switch between them with a button attached to the back of the Light.