#include "FastLED.h"

#define NUM_LEDS 16
#define DATA_PIN 3
#define BTN_PIN 4
#define COLOR_ORDER GRB

// Define the array of leds
CRGB leds[NUM_LEDS];

// This array is the pattern in which the seconds will be updated. It is shuffled after it is done
uint8_t idxSeconds[12] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}; // TODO: shouldn't be hardcoded
uint8_t count_idxSeconds = 0;
uint8_t saturationSeconds = 255;
uint32_t secondsToday = 0;
unsigned long time_exec = 0;
unsigned long buttonLastPressTime = 0;
unsigned long timeLastAction = 0;
uint32_t state = 0;
bool updateClock = false;


#define BRIGHTNESS        90
#define FRAMES_PER_SECOND 120
#define BUTTON_BUFFER_TIME 500

enum Mode {
  RAINBOW,
  RAINBOW_GLITTER,
  SNAKE,
  RANDOM_ALL,
  BOUNCE,
  RANDOM_DOT,
  FLIMMER,
  FILL_RANDOM,
  CLOCK
};


enum Mode currentMode = RANDOM_DOT;


void setup() {
  FastLED.addLeds<WS2812B, DATA_PIN, COLOR_ORDER>(leds, NUM_LEDS);
  time_exec = millis();

  // set master brightness control
  FastLED.setBrightness(BRIGHTNESS);

  Serial.begin(9600);      // open the serial port at 9600 bps:

  pinMode(BTN_PIN, INPUT);
  digitalWrite(BTN_PIN, HIGH);      // turn on pullup resistor
}

void loop() {
  // insert a delay to keep the framerate modest
  FastLED.delay(1000 / FRAMES_PER_SECOND);

  updateTime();
  switch (currentMode) {
    case RAINBOW: rainbow(false); break;
    case RAINBOW_GLITTER: rainbow(true); break;
    case SNAKE: snake(); break;
    case RANDOM_ALL: randomAll(); break;
    case BOUNCE: bounce(); break;
    case RANDOM_DOT: randomDot(); break;
    case FLIMMER: flimmer(); break;
    case FILL_RANDOM: fillRandom(); break;
    case CLOCK: break;
    default: Serial.println("error");
  }
  checkButton();
}

void checkButton() {
  if (buttonPressed()) {
    switch (currentMode) {
      case RAINBOW: currentMode = RAINBOW_GLITTER; break;
      case RAINBOW_GLITTER: currentMode = SNAKE; break;
      case SNAKE: currentMode = RANDOM_ALL; break;
      case RANDOM_ALL: timeLastAction = millis(); currentMode = BOUNCE; break;
      case BOUNCE: currentMode = FLIMMER; break;
      case FLIMMER: currentMode = FILL_RANDOM; break;
      case FILL_RANDOM: currentMode = RANDOM_DOT;  break;
      case RANDOM_DOT: currentMode = CLOCK; initLEDSclock(); updateClock = true; break;
      case CLOCK: currentMode = RAINBOW; updateClock = false; break;
      default: Serial.println("error");
    }
  }
}

void rainbow(boolean hasGlitter) {
  static uint8_t hue_rainbow = 0;
  if (millis() > timeLastAction + 40) {
    fill_rainbow( leds, NUM_LEDS, hue_rainbow++, 7);
    if (hasGlitter) {
      if ( random8() < 80) {
        leds[ random16(NUM_LEDS) ] += CRGB::White;
      }
    }
    timeLastAction = millis();
  }
}

void snake() {
  static uint8_t hue_snake = 0;
  if (millis() > timeLastAction + 250) {
    if (state < NUM_LEDS) {
      leds[state] = CHSV(hue_snake++, 255, 255);
    } else {
      leds[NUM_LEDS - (state % NUM_LEDS)] = CHSV(hue_snake++, 255, 255);
    }
    state++;
    if (state == 2 * NUM_LEDS) {
      state = 0;
    }
    fadeall(230);
    FastLED.show();
    timeLastAction = millis();
  }
}

void randomDot() {
  if (millis() > timeLastAction) {
    leds[random(NUM_LEDS)] = CHSV(random(255), 255, 255);
    FastLED.show();
    // Period needs to be added in the end, so something happens immediately
    timeLastAction = millis() + 1000; 
  }
}

void flimmer() {
  static uint8_t series[NUM_LEDS] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15}; // TODO: shouldn't be hardcoded
  static uint8_t currentHue = random(255);
  static uint8_t count = NUM_LEDS;

  if (millis() > timeLastAction + 150) {
    if (count >= NUM_LEDS) {
      count = 0;
      shuffleArray(series, NUM_LEDS);
      state = (state + 1) % 2;
      currentHue = random(255);
    }

    if (state != 0) {
      // Build up color
      leds[series[count++]] = CHSV(currentHue, 255, 255);
    } else {
      // Reduce color
      leds[series[count++]] = CHSV(currentHue, 255, 175);
    }
    timeLastAction = millis();
  }
}

void randomAll() {
  if (millis() > timeLastAction) {
    for (int i = 0; i < NUM_LEDS; i++) {
      leds[i] = CHSV(random(255), 255, random(125, 255));
    }
      FastLED.show();
    // Period needs to be added in the end, so something happens immediately
      timeLastAction = millis() + 5000;
  }
}

void bounce() {
  if (millis() > timeLastAction + 1) {
    fadeToBlackBy( leds, NUM_LEDS, 50);
    byte dothue = 0;
    for ( int i = 0; i < 8; i++) {
      leds[beatsin16( i + 7, 0, NUM_LEDS - 1 )] |= CHSV(dothue, 255, 255);
      dothue += 32;
    }
    timeLastAction = millis();
  }
}

void fillRandom() {
  static uint8_t hue_fill_random = 0;
  static long timeNextAction = 0;
  static int step = 0;
  if (millis() > timeNextAction) {
    step = 1 - 2 * random(0, 2);
    timeNextAction = millis() + random(0, 8000);
  }
  if (millis() > timeLastAction + 80) {
    hue_fill_random += step;
    fill_solid( leds, NUM_LEDS, CHSV(hue_fill_random, 255, 255));
    timeLastAction = millis();
  }
}


void runClock() {
  updateSecondsLED();
  updateHoursLED();
  FastLED.show();
}


void updateTime() {
  if (millis() > time_exec) {
    printTime();
    time_exec = millis() + 1000;

    secondsToday = (secondsToday + 1) % 86400; // modulo to start counting at zero after a day
    if (updateClock) runClock();
  }
}


void updateSecondsLED() {
  uint8_t minutes = (secondsToday / 60) % 60;

  // Prepare all colors
  uint8_t hue = (255.0 / 59) * minutes;  // 59 since minutes range from 0 to 59
  CHSV newColor = CHSV(hue, saturationSeconds, 255);

  // The next index to be updated is written in idxSeconds
  leds[idxSeconds[count_idxSeconds]] = newColor;

  updateIdxSeconds();
}

// The leds for the second are picked randomly from an array.
// This function checks if all numbers have been picked and in that case updates the array
void updateIdxSeconds() {
  count_idxSeconds++;
  if (count_idxSeconds == NUM_LEDS - 4) {
    // Shuffle array and reset counter
    count_idxSeconds = 0;
    shuffleArray(idxSeconds, NUM_LEDS - 4);

    // Change saturation
    if (saturationSeconds == 255) {
      saturationSeconds = 125;
    } else {
      saturationSeconds = 255;
    }
  }
}

void updateHoursLED() {
  uint8_t hours = (secondsToday / 3600) % 12; // am and pm doesn't matter

  // Get colors and index
  uint8_t hue = (180.0 / 2) * (int)(hours / 4);
  // hours 0, 4 and 8 need to have the last color, not the new current one
  if (hours % 4 == 0) {
    uint8_t lastHour = (hours + 12 - 1) % 12;
    hue = (180.0 / 2) * (int)(lastHour / 4);
  }
  uint8_t led_index = NUM_LEDS - 4 + (hours + 12 - 1) % 4;
  leds[led_index] = CHSV(hue, 255, 255);
}

void initLEDSclock() {
  // Get second colors
  uint8_t minutes = (secondsToday / 60) % 60;
  uint8_t hueSeconds = (255.0 / 59) * minutes;  // 59 since minutes range from 0 to 59
  for (int i = 0; i < NUM_LEDS - 4; ++i) {
    leds[i] = CHSV(hueSeconds, 128, 255);
  }

  // Set hour colors
  uint8_t hours = (secondsToday / 3600) % 12; // am and pm doesn't matter
  
  // Print all hours in color of previous hours
  uint8_t hueLastPeriod = 90 * ((hours + 12 - 4) % 12 / 4);
  for (int i = NUM_LEDS - 4; i < NUM_LEDS; ++i) {
    leds[i] = CHSV(hueLastPeriod, 255, 255);
  }

  // Print all current hours with the color of the current hour
  uint8_t currentIdx = hours % 4;
  uint8_t currentHue = (hueLastPeriod + 90) % 360;
  for (int i = NUM_LEDS - 4; i < NUM_LEDS - (4-currentIdx); ++i) {
    leds[i] = CHSV(currentHue, 255, 255);
  }
}

void printTime() {
  uint8_t seconds = secondsToday % 60;
  uint8_t minutes = (secondsToday / 60) % 60;
  uint8_t hours = secondsToday / 3600;
  Serial.print(hours);
  Serial.print(":");
  Serial.print(minutes);
  Serial.print(":");
  Serial.println(seconds);
}

bool buttonPressed() {
  if (millis() > buttonLastPressTime) {
    if (digitalRead(BTN_PIN) == HIGH) {
      buttonLastPressTime = millis() + BUTTON_BUFFER_TIME;
      return true;
    }
  }
  return false;
}

void allWhite() {
  fill_solid( leds, NUM_LEDS, CRGB(255, 255, 255));
}

void setColor(CRGB col) {
  fill_solid( leds, NUM_LEDS, col);
}

void shuffleArray(uint8_t * array, int size)
{
  uint8_t last = 0;
  uint8_t temp = array[last];
  for (int i = 0; i < size; i++)
  {
    int index = random(size);
    array[last] = array[index];
    last = index;
  }
  array[last] = temp;
}


void fadeall(int strength) {
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i].nscale8(strength);
  }
}
